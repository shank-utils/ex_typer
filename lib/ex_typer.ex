defmodule ExTyper do
  @behaviour Ratatouille.App

  import Ratatouille.View

  @tmp_text "But in darkness they came, through stormy black seas they raided these shores. Do you still hear his screams? And now that you're home he's so far away. They've taken his soul. To these gods you cannot pray. They can break you, but not your promise. Even death won't keep you apart. Through this darkness you will find him. In your sword still beats a heart."

  def init(_context), do: 0

  def update(count, msg) do
    case msg do
      {:event, %{ch: ?+}} -> count + 1
      {:event, %{ch: ?-}} -> count - 1
      {:event, %{ch: ?*}} -> count * count
      _ -> count
    end
  end

  def render(count) do
    view do
      panel(title: "test") do
        label do
          text(content: @tmp_text, color: :red)
        end

        label()

        table do
          table_row do
            table_cell(content: @tmp_text)
          end
        end
      end

      label(content: "Counter is #{count} (+/-)")
    end
  end
end
